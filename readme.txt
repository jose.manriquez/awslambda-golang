Fuente: https://www.javacodegeeks.com/2018/11/build-restful-api-go-using-aws-lambda.html

$env:GOOS="linux"
$env:GOARCH="amd64"

aws iam get-role --role-name FindAllMoviesRole

build-lambda-zip.exe -o <salida.zip> <ejecutable>

aws lambda create-function --function-name InsertMovie --zip-file fileb://main.zip  --runtime go1.x --handler main --role arn:aws:iam::<ID>:role/InsertMovieRole --region us-east-1